### Reactive workshop Scala starter

Follow [the steps](https://gist.github.com/arkadijs/a2a6901d34272fbbdaa7) and run the starter:

    $ ./sbt
    > run

First run will be slow - the dependencies must be downloaded.
