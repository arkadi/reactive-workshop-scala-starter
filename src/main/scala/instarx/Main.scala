package instarx

import scala.collection.JavaConversions._
import scala.util.control.Breaks._
import scala.concurrent.ExecutionContext.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.{read, write}
import rx.lang.scala.{Observable, Observer, Subscriber, Subscription}
import rx.lang.scala.subjects.ReplaySubject
import rx.lang.scala.schedulers.NewThreadScheduler
import dispatch._, Defaults._

object Main {

  val log = org.slf4j.LoggerFactory.getLogger(getClass)

  val participant = Some("change-me")
  val clientId = "" // obtain your own at https://instagram.com/developer
  val api = "https://api.instagram.com/v1"
  val ui = "http://rxdisplay.neueda.lv/in"
  val interval = 5.seconds
  val count = 10 // count of tagged media to return
  val tags = "invent your own".split(" ").toSeq
  val limit = 3 // number of tags per observable stream
  val scheduler = NewThreadScheduler()

  case class Location(latitude: Option[Double], longitude: Option[Double], id: Option[BigInt], name: Option[String])
  case class Media(tag: String, url: String, location: Option[Location], participant: Option[String] = None)

  private def unjson(text: String)(implicit tag: String): Seq[Media] = {
    // Parse Instagram media response to extract image URL and location (if any)
  }

  def main(args: Array[String]) {
    // step one: multi-threaded HTTP fetch
    val ticker = Observable.*****
    val obstags = Observable.*****(tags.take(limit))
    val nested/*: Observable[Observable[Seq[Media]]]*/ = ticker.map { _ =>
      obstags.***** { implicit tag =>
        // http://dispatch.databinder.net/Combined+Pages.html
        val future/*: Future[Seq[Media]]*/ = Http(*****).map { resp =>
          *****
        } // and maybe errror recovery
        Observable.*****(future)
      }
    }
    val list/*: Observable[Seq[Media]]*/ = nested.*****

    list.subscribe(/* print */)

    // step two: flatten
    val instagram = Observable((subscriber: Subscriber[Media]) => {
      list.subscribe(3-args)
    })

    // step three: send to UI
    instagram.subscribe { image =>
      *****
      val req = *****(image.copy(participant = participant))
      Http(req.*****)
    }

    // step four: deduplicate and verify
    val start = System.currentTimeMillis
    val limited = instagram.takeWhile(*****)
    limited.size.foreach(*****)
    *****


    Thread.sleep(10000)
  }
}
